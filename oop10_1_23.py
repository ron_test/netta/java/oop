from itertools import permutations
##oop - 2


class SubsetGenerator:
    def __init__(self, num_set):
        self.num_set = num_set
        self.subsets = []

    def possible_unique_subsets(self, num_set):
        self.subsets = list(permutations(self.num_set))
        return print(self.subsets)


def main():
    input_set = [4, 5, 6]
    obj = SubsetGenerator(input_set)
    print(obj.possible_unique_subsets(input_set))


if __name__ == '__main__':
    main()



## oop - 3
class Reverse:
    def __init__(self, string):
        self.string = string

    def to_reverse(self):
        words = self.string.split()
        reversed_words = words[::-1]
        self.string = ' '.join(reversed_words)
        return self.string


def main():
    string_input = "hello how are you"
    obj = Reverse(string_input)
    print(obj.to_reverse())


if __name__ == '__main__':
    main()

